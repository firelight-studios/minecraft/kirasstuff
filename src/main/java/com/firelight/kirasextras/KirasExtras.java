package com.firelight.kirasextras;

import com.firelight.kirasextras.block.ModBlocks;
import com.firelight.kirasextras.item.ModItemGroups;
import com.firelight.kirasextras.item.ModItems;
import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KirasExtras implements ModInitializer {
    public static final String MOD_ID = "kirasextras";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    @Override
    public void onInitialize() {
        ModItemGroups.registerItemGroups();
        ModItems.registerModItems();
        ModBlocks.registerModBlocks();
    }
}
