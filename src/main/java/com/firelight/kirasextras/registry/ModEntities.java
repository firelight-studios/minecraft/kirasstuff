package com.firelight.kirasextras.registry;

import com.firelight.kirasextras.KirasExtras;
import com.firelight.kirasextras.entity.PipeBombEntity;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModEntities {
    public static final EntityType<PipeBombEntity> PipeBomb = Registry.register(
            Registries.ENTITY_TYPE, new Identifier(KirasExtras.MOD_ID, "pipe_bomb"),
            FabricEntityTypeBuilder.<PipeBombEntity>create(SpawnGroup.MISC, PipeBombEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).build()
    );

    private static EntityType<?> register(String name) {
        //TODO: This method
        return null;
    }
}
