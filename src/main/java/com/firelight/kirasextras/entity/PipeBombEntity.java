package com.firelight.kirasextras.entity;

import com.firelight.kirasextras.KirasExtras;
import com.firelight.kirasextras.registry.ModEntities;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.impl.networking.v0.ServerSidePacketRegistryImpl;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.thrown.ThrownEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

import java.util.UUID;

public class PipeBombEntity extends ThrownEntity {
    public static final Identifier SPAWN_PACKET = new Identifier(KirasExtras.MOD_ID, "pipe_bomb");

    public PipeBombEntity(EntityType<? extends ThrownEntity> entityType, World world) {
        super(entityType, world);
    }

    @Environment(EnvType.CLIENT)
    public PipeBombEntity(World world, double x, double y, double z, int id, UUID uuid) {
        super(ModEntities.PipeBomb, world);
        updatePosition(x, y, z);
        setId(id);
        setUuid(uuid);
    }

    @Override
    protected void initDataTracker() {

    }

    @Override
    public Packet<ClientPlayPacketListener> createSpawnPacket() {
        PacketByteBuf packet = new PacketByteBuf(Unpooled.buffer());

        // pos
        packet.writeDouble(getX());
        packet.writeDouble(getY());
        packet.writeDouble(getZ());

        // id & uuid
        packet.writeInt(getId());
        packet.writeUuid(getUuid());

        return (Packet<ClientPlayPacketListener>) ServerSidePacketRegistryImpl.INSTANCE.toPacket(SPAWN_PACKET, packet);
    }
}
