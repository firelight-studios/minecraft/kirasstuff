package com.firelight.kirasextras.item.Custom;

import com.firelight.kirasextras.entity.PipeBombEntity;
import com.firelight.kirasextras.registry.ModEntities;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SnowballItem;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class PipeBombItem extends SnowballItem {
    public PipeBombItem(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack itemStack = user.getStackInHand(hand);
        world.playSound(null, user.getX(), user.getY(), user.getZ(), SoundEvents.ENTITY_SNOWBALL_THROW, SoundCategory.NEUTRAL, 0.5f, 1);

        if (!world.isClient) {
            PipeBombEntity proj = new PipeBombEntity(ModEntities.PipeBomb, world);
            proj.setOwner(user);
            proj.setPitch(user.getPitch());
            proj.setYaw(user.getYaw());

        }

        if (!user.getAbilities().creativeMode) {
            itemStack.decrement(1);
        }
    }
}
