package com.firelight.kirasextras.item;

import com.firelight.kirasextras.KirasExtras;
import com.firelight.kirasextras.block.ModBlocks;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class ModItemGroups {
    public static final ItemGroup KIRA_GROUP = Registry.register(Registries.ITEM_GROUP,
            new Identifier(KirasExtras.MOD_ID, "extras"),
            FabricItemGroup.builder().displayName(Text.translatable("itemgroup.extra"))
                    .icon(() -> new ItemStack(ModItems.RUBY)).entries(((displayContext, entries) -> {
                        entries.add(ModItems.RUBY);

                        entries.add(ModBlocks.RUBY_BLOCK);
                        entries.add(ModBlocks.RUBY_ORE);
                    })).build());

    public static void registerItemGroups() {

    }
}
