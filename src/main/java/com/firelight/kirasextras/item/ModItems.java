package com.firelight.kirasextras.item;

import com.firelight.kirasextras.KirasExtras;
import com.firelight.kirasextras.item.Custom.PipeBombItem;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroupEntries;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class ModItems {
    public static final Item RUBY = registerItem("ruby", new Item(new FabricItemSettings()));

    public static final Item PIPE_BOMB = registerItem("pipe_bomb", new PipeBombItem(new FabricItemSettings()));

    private static void addItemsToIngredientGroup(FabricItemGroupEntries entries) {
        entries.add(RUBY);
    }

    private static Item registerItem(String name, Item item) {
        return Registry.register(Registries.ITEM, new Identifier(KirasExtras.MOD_ID, name), item);
    }

    public static void registerModItems() {
        KirasExtras.LOGGER.info(KirasExtras.MOD_ID + ": Registering items");

        //ItemGroupEvents.modifyEntriesEvent(ItemGroups.INGREDIENTS).register(ModItems::addItemsToIngredientGroup);
    }
}
