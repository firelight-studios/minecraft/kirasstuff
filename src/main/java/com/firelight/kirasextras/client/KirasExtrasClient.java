package com.firelight.kirasextras.client;

import com.firelight.kirasextras.client.renderer.PipeBombEntityRenderer;
import com.firelight.kirasextras.entity.PipeBombEntity;
import com.firelight.kirasextras.registry.ModEntities;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.impl.client.networking.v0.ClientSidePacketRegistryImpl;
import net.minecraft.client.MinecraftClient;

import java.util.UUID;

public class KirasExtrasClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(ModEntities.PipeBomb, PipeBombEntityRenderer::new);


        ClientSidePacketRegistryImpl.INSTANCE.register(PipeBombEntity.SPAWN_PACKET, ((context, buffer) -> {
            double x = buffer.readDouble();
            double y = buffer.readDouble();
            double z = buffer.readDouble();

            int id = buffer.readInt();
            UUID uuid = buffer.readUuid();

            context.getTaskQueue().execute(() -> {
                PipeBombEntity proj = new PipeBombEntity(MinecraftClient.getInstance().world, x, y, z, id, uuid);
                MinecraftClient.getInstance().world.addEntity(id, proj);
            });
        }));
    }
}
