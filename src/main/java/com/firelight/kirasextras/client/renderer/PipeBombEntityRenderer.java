package com.firelight.kirasextras.client.renderer;

import com.firelight.kirasextras.entity.PipeBombEntity;
import com.firelight.kirasextras.item.ModItems;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class PipeBombEntityRenderer extends EntityRenderer<PipeBombEntity> {
    public static final ItemStack STACK = new ItemStack(ModItems.PIPE_BOMB);

    public PipeBombEntityRenderer(EntityRendererFactory.Context ctx) {
        super(ctx);
    }

    @Override
    public void render(PipeBombEntity entity, float yaw, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light) {
        MinecraftClient.getInstance().getItemRenderer().renderItem(
                STACK,
                ModelTransformationMode.FIXED,
                light,
                0,
                matrices,
                vertexConsumers,
                entity.getWorld(),
                0
        );

        super.render(entity, yaw, tickDelta, matrices, vertexConsumers, light);
    }

    @Override
    public Identifier getTexture(PipeBombEntity entity) {
        return null;
    }
}
